resource "kubernetes_deployment_v1" "task" {
  metadata {
    name = "task"
    labels = {
      test = "task"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "task"
      }
    }

    template {
      metadata {
        labels = {
          test = "task"
        }
      }

      spec {
        container {
          image = "350726766717.dkr.ecr.us-east-1.amazonaws.com/task-service-3:latest"
          name  = "task"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "task_svc" {
  metadata {
    name = "task"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.task.metadata.0.labels.test
    }
    
    port {
      port        = 8000
      target_port = 8000
    }

    type = "NodePort"
  }
}
